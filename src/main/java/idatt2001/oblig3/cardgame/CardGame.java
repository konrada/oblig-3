package idatt2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.text.Text;

public class CardGame extends Application{

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {
        DeckOfCards deck = new DeckOfCards();
        final HandOfCards[] hand = {new HandOfCards()};
        primaryStage.setTitle("Card Game");
        Button dealHand = new Button("Deal Hand");
        Button checkHand = new Button("Check Hand");

        StackPane playingField = new StackPane();
        playingField.setStyle("-fx-border-color: black");
        playingField.setMinSize(300,300);

        Text sumFaces = new Text("Sum of faces");
        StackPane sumFacesField = new StackPane();
        sumFacesField.setStyle("-fx-border-color: black");
        sumFacesField.setMinSize(20, 20);

        Text cardsHearts = new Text("Cards of hearts");
        StackPane cardsHeartsField = new StackPane();
        cardsHeartsField.setStyle("-fx-border-color: black");
        cardsHeartsField.setMinSize(20, 20);

        Text flushText = new Text("Flush: ");
        StackPane flushField = new StackPane();
        flushField.setStyle("-fx-border-color: black");
        flushField.setMinSize(20, 20);

        Text queenText = new Text("Queen of Spades? ");
        StackPane queenField = new StackPane();
        queenField.setStyle("-fx-border-color: black");
        queenField.setMinSize(20, 20);

        dealHand.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                playingField.getChildren().clear();
                hand[0] = deck.dealHand(5);
                Text cards = new Text(hand[0].toString());
                playingField.getChildren().add(cards);
            }
        });

        checkHand.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sumFacesField.getChildren().clear();
                cardsHeartsField.getChildren().clear();
                queenField.getChildren().clear();
                flushField.getChildren().clear();
                Text sum = new Text(String.valueOf(hand[0].getSum()));
                sumFacesField.getChildren().add(sum);
                Text hearts = new Text(hand[0].getHearts());
                cardsHeartsField.getChildren().add(hearts);
                Text queen = new Text(hand[0].checkForQueenOfSpades());
                queenField.getChildren().add(queen);
                Text flush = new Text(hand[0].checkForFlush());
                flushField.getChildren().add(flush);
            }
        });


        VBox vbox = new VBox(8);
        vbox.getChildren().add(playingField);
        vbox.getChildren().add(dealHand);
        vbox.getChildren().add(checkHand);
        vbox.getChildren().add(sumFaces);
        vbox.getChildren().add(sumFacesField);
        vbox.getChildren().add(cardsHearts);
        vbox.getChildren().add(cardsHeartsField);
        vbox.getChildren().add(queenText);
        vbox.getChildren().add(queenField);
        vbox.getChildren().add(flushText);
        vbox.getChildren().add(flushField);

        primaryStage.setScene(new Scene(vbox, 900, 600));
        primaryStage.show();
    }
    @Override
    public void init() throws Exception {
        super.init();
    }
    @Override
    public void stop() throws Exception {
        super.stop();
    }
}
