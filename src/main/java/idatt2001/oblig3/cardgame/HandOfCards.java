package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {
    private ArrayList<PlayingCard> cardsInHand;

    public HandOfCards(ArrayList<PlayingCard> cardsInHand) {
        this.cardsInHand = cardsInHand;
    }
    public HandOfCards() {

    }
    public String toString() {
        String res = "";
        for (PlayingCard p : cardsInHand) {
            res+= p.getAsString() + "     ";
        }
        return res;
    }
    public int getSum() {
        return cardsInHand.stream().map(PlayingCard::getFace).reduce((a, b) -> a+b).get();
    }

    public String getHearts() {
        String res = "";
        List list =  cardsInHand.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toList());
        for (Object o : list) {
            PlayingCard p = (PlayingCard) o;
            res += p.getAsString() + "     ";
        }
        if (res.equals("")) {
            res = "No hearts";
        }
        return res;
    }
    public String checkForQueenOfSpades() {
        List list = cardsInHand.stream().filter(p -> p.getSuit() == 'S' && p.getFace() == 12).collect(Collectors.toList());
        if (list.isEmpty()) {
            return "No Queen of Spades";
        } else {
            return "Hand contains queen of spades";
        }
    }
    public String checkForFlush() {
        char[] suit = { 'S', 'H', 'D', 'C' };
        for (char h: suit) {
            List list = cardsInHand.stream().filter(p -> p.getSuit() == h).collect(Collectors.toList());
            if (list.size() == 5) {
                return "Flush";
            }
        }
        return "No flush";
    }

}
