package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> cards = new ArrayList<PlayingCard>();

    public DeckOfCards() {
        for(char c : suit) {
            for (int i = 1; i < 14; i++) {
                cards.add(new PlayingCard(c, i));
            }
        }
    }
    public HandOfCards dealHand(int n) {
        ArrayList<PlayingCard> cardsToHand = new ArrayList<PlayingCard>();
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int r = random.nextInt(cards.size());
            cardsToHand.add(cards.get(r));
        }
        HandOfCards hand = new HandOfCards(cardsToHand);
        return hand;
    }

    public String toString() {
        String res = "";
        for (PlayingCard p : cards) {
            res += p.getAsString() + "\n";
        }
        return res;
    }
}
